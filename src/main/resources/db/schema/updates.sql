drop index tgis_management_unit_region_id_mu_code_idx;

CREATE UNIQUE INDEX tgis_management_unit_region_id_mu_code_idx ON public.tgis_management_unit USING btree (region_id, mu_code, mu_name);